const mongoose = require('mongoose');

const contactSchema = new mongoose.Schema({
  lastname: {
    type: String,
    required: true,
  },
  firstname: {
    type: String,
    required: true,
  },
  patronymic: String,
  phone: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: Date,
});

contactSchema.virtual('id').get(function () {
    return this._id;
})

const Contact = mongoose.model('Contact', contactSchema);

module.exports = Contact;
