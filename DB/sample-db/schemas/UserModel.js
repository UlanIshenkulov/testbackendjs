const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

userSchema.virtual('id').get(function () {
    return this._id;
})

const Contact = mongoose.model('User', userSchema);

module.exports = Contact;