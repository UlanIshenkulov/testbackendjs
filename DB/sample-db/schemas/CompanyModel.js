const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
  contactId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Contact',
  },
  name: {
    type: String,
    required: true,
  },
  shortName: String,
  businessEntity: String,
  contract: {
    no: String,
    issue_date: Date,
  },
  type: {
    type: [String],
    default: [],
  },
  status: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'active',
  },
  address: {
    street: String,
    city: String,
    postalCode: String,
    country: String,
  },
  photos: {
    type: [{
      name: String,
      filepath: String,
      thumbpath: String,
    }],
    default: [],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: Date,
});

companySchema.virtual('id').get(function () {
    return this._id;
})

const Company = mongoose.model('Company', companySchema);

module.exports = Company;

