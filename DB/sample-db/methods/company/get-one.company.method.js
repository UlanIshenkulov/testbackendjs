const Company = require('../../schemas/CompanyModel');

/**
 * Возвращает данные компании с указанным идентификатором.
 * @param {string} id
 * @return {Object|null}
 */
async function getOne(id) {
  return Company.findOne({_id:id});
}

module.exports = { getOne };
