const Company = require('../../schemas/CompanyModel');

/**
 * Редактирует данные компании с указанным идентификатором
 * и возвращает результат.
 * @param {string} id
 * @param {Object} data
 * @return {Object}
 */
async function editOne(id, data,company) {
  data.updatedAt = new Date();
  return Company.findByIdAndUpdate(
    id,
    { $set: data },
    { new: true }
  );
}

module.exports = { editOne };
