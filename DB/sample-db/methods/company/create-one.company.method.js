const Company = require('../../schemas/CompanyModel');

async function createCompany(value) {
    return Company.create({
        contactId: value.contactId,
        name:value.name,
        shortName:value.shortName,
        businessEntity:value.businessEntity,
        contract:value.contract,
        type:value.type,
        address:value.address,
        status:value.status
    })
}
  
  module.exports = { createCompany };