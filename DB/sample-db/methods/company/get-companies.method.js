const Company = require('../../schemas/CompanyModel');
const { parseOne } = require("../../../../routes/companies/companies.service");

async function getCompanies(perPage,page,filter) {
   return Company.find(filter).skip(perPage * (page - 1)).limit(perPage);
}

function createFilter(name,createdAt,status,type) {
  const filter = {};

  if(name !== null) {
    filter.name = name;
  };

  if (createdAt !== null) {
    const createdAtDate = new Date(createdAt);
    const nextDay = new Date(createdAtDate);
    nextDay.setDate(createdAtDate.getDate() + 1);
    filter.createdAt =  {
        $gte: createdAtDate,
        $lt: nextDay,
      }
    };

  if(status !== null) {
    filter.status = status;
  }

  if(type.length > 0) {
    filter.type = { $in: type };
  }

  return filter;
}

function parseCompaniesPhotoUrl(companies,photoUrl) {
    companies.forEach((company) => {
        if(company.photos.length > 0) {
            return parseOne(company,photoUrl);
          }else {
            return company;
          }
    })
};

module.exports = { getCompanies, createFilter, parseCompaniesPhotoUrl };