const Company = require('../../schemas/CompanyModel');

async function deleteOne(id) {
  return Company.findByIdAndDelete(id);
}

module.exports = { deleteOne };