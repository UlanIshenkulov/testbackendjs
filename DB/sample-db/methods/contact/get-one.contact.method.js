const Contact = require('../../schemas/ContactModel');

async function getOne(id) {
  return Contact.findOne({_id:id});
}

async function isContactExists(email) {
  const contact = await Contact.findOne({ email });
  return !!contact;
}

module.exports = { getOne, isContactExists };
