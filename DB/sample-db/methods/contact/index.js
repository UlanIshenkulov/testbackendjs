module.exports = {
  ...require("./get-one.contact.method"),
  ...require("./edit-one.contact.method"),
  ...require("./create-contact.method"),
  ...require("./delete-contact.method"),
};
