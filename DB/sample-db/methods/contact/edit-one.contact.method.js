const Contact = require("../../schemas/ContactModel");

/**
 * Редактирует данные контакта с указанным идентификатором
 * и возвращает результат.
 * @param {string} id
 * @param {Object} data
 * @return {Object}
 */
async function editOne(id, data) {
  data.updatedAt = new Date();
  return Contact.findByIdAndUpdate(
    id,
    { $set: data },
    { new: true }
  );
}

module.exports = { editOne };
