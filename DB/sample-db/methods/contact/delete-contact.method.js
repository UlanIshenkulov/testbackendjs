const Contact = require('../../schemas/ContactModel');
const Company = require('../../schemas/CompanyModel');

async function deleteOne(id) {
  const deletedContact =  Contact.findByIdAndDelete(id);

  if (!deletedContact) {
    return null; // Contact not found;
  }
  // i want to delete all associated companies;
  await Company.deleteMany({ contactId: id });
  return deletedContact;
}

module.exports = { deleteOne };