const Contact = require('../../schemas/ContactModel');

async function createContact(value) {
    return Contact.create({
        lastname: value.lastname,
        firstname:value.firstname,
        patronymic:value.patronymic,
        phone:value.phone,
        email:value.email,
    })
}
  
  module.exports = { createContact };
