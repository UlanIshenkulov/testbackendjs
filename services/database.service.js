const dbsConfig = require("../config").dbs;
const logger = require("./logger.service")(module);
const mongoose = require('mongoose');

/**
 * Базовый класс сервиса работы с базой данных
 */
class Database {
  #uri;

  #id;

  #database;

  #client;

  constructor(config) {
    this.#uri = config.uri;
    this.#id = config.id;
    this.#database = config.database;
    this.#client = null;
  }

  /**
   * Открывает соединение с БД.
   * @return {Promise<void>}
   */
  async connect() {
    try {
      this.#client = await mongoose.connect(this.#uri);
      logger.info(`Connected to ${this.#id}`);
    } catch (error) {
      logger.error(`Unable to connect to ${this.#id}:`, error.message);
    }
  }

  /**
   * Закрывает соединение с БД.
   * @return {Promise<void>}
   */
  async disconnect() {
    if (this.#client) {
      try {
        await this.#client.close();
        logger.info(`Disconnected from ${this.#id}`);
      } catch (error) {
        logger.error(`Unable to disconnect from ${this.#id}:`, error.message);
      }
    }
  }

  /**
   * Возвращает объект соединения с БД,
   * @return {Object}
   */
  getConnection() {
    return this.#client && this.#client.db(this.#database);
  }

  isConnected() {
    return mongoose.connection.readyState === 1;
  }
}

const sampleDB = new Database(dbsConfig.sample_db);

module.exports = { sampleDB };
