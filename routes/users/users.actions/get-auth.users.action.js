const logger = require("../../../services/logger.service")(module);
const User = require("../../../DB/sample-db/schemas/UserModel");
const { OK } = require("../../../constants/http-codes");
const { NotFound, BadRequest } = require("../../../constants/errors");
const JwtService = require("../../../services/jwt.service");
const jwtConfig = require("../../../config").jwt;
const bcrypt = require('bcrypt');
const Joi = require('joi');

async function getAuth(req, res) {
  logger.init("get user auth");

  const schema = Joi.object({
    name: Joi.string().required(),
    password: Joi.string().required(),
  });

  const {value, error} = schema.validate(req.body);

  if (error) {
     throw new BadRequest(error.details[0].message );
   }

  const name = value.name;
  const password = value.password;

  if (name !== 'admin') {
    throw new NotFound("User is not found");
  };

  let user = await User.findOne({name});

  if(!user) {
    const hashedPassword = await bcrypt.hash(password, 10);
    user = await User.create({
      name,
      password:hashedPassword
    });
  }else {
    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      throw new BadRequest("password is not correct");
    }
  };
  
  const token = new JwtService(jwtConfig).encode(user).data;

  res.header("Authorization", `Bearer ${token}`);
  logger.success();
  // я для себя еще token добавил в response;
  return res.status(OK).json({user, token});
}

module.exports = {
  getAuth,
};
