const multer = require("multer");
const { Router } = require("express");
const actions = require("./companies.actions");
const validator = require("./companies.validator");
const config = require("../../config");

const fileHandler = multer({ dest: config.images.uploadsDir });

module.exports = Router()
  .get("/companies/:id", actions.getOne)
  .post("/companies/",actions.createCompany)
  .get("/companies/",actions.getCompanies)
  .patch("/companies/:id", actions.editOne)
  .delete("/companies/:id",actions.deleteOne)
  .post(
    "/companies/:id/image",
    fileHandler.fields([{ name: "file", maxCount: 1 }]),
    ...validator.addImage,
    actions.addImage
  )
  .delete(
    "/companies/:id/image",
    ...validator.removeImage,
    actions.removeImage
  );
