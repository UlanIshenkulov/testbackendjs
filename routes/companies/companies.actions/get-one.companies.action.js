const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const companyMethods = require("../../../DB/sample-db/methods/company");
const { getUrlForRequest } = require("../../../helpers/url.helper");
const { NotFound, BadRequest } = require("../../../constants/errors");
const { parseOne } = require("../companies.service");
const Joi = require('joi');


/**
 * GET /companies/:id
 * Эндпоинт получения данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function getOne(req, res) {
  logger.init("get company");

  const schema = Joi.object({
    id: Joi.string().length(24).regex(/[0-9a-f]{24}/).required(),
  });

  const {value, error} = schema.validate(req.params);

  if (error) {
    throw new BadRequest(error.details[0].message );
  }
  const company = await companyMethods.getOne(value.id);

  if (!company) {
    throw new NotFound("Company not found");
  }
  const photoUrl = getUrlForRequest(req);

  if(company.photos.length > 0) {
    res.status(OK).json(parseOne(company, photoUrl));
  }else {
    res.status(OK).json(company);
  }
  logger.success();
}

module.exports = {
  getOne,
};
