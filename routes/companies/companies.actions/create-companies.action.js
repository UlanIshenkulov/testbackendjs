const logger = require("../../../services/logger.service")(module);
const { CREATED } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/sample-db/methods/contact");
const companyMethods = require("../../../DB/sample-db/methods/company");
const { BadRequest, NotFound } = require("../../../constants/errors");
const Joi = require('joi');

/**
 * POST /companies/
 * Эндпоинт создания компании
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function createCompany(req, res) {
  logger.init("create company");

  const schema = Joi.object({
    contactId: Joi.string().length(24).regex(/[0-9a-f]{24}/).required(),
    name: Joi.string().min(3).max(20).required(),
    shortName: Joi.string().min(3).max(10),
    businessEntity: Joi.string().required(),
    status: Joi.string().valid('active', 'inactive'),
    contract: Joi.object({
      no: Joi.string().required(),
      issue_date: Joi.string().isoDate().required(),
    }).required(),
    type: Joi.array().items(Joi.string().valid('agent', 'contractor')).required(),
    address:Joi.object({
        street: Joi.string().required().min(3).max(20),
        city: Joi.string().required().min(3).max(20),
        postalCode: Joi.string().min(3).max(20),
        country: Joi.string().required().min(3).max(20),
      }).required()
  });

   const {value, error} = schema.validate(req.body);

   if (error) {
       throw new BadRequest(error.details[0].message );
    }

  const contact = await contactMethods.getOne(value.contactId);
  
  if(!contact) {
    throw new NotFound( 'Contact does not found' );
  }

  const newCompany = await companyMethods.createCompany(value);

  res.status(CREATED).json(newCompany);
  logger.success();
}

module.exports = {
    createCompany,
};
