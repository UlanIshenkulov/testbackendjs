const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const companyMethods = require("../../../DB/sample-db/methods/company");
const { getUrlForRequest } = require("../../../helpers/url.helper");
const { BadRequest } = require("../../../constants/errors");
const { parseOne } = require("../companies.service");
const Joi = require('joi');
const Company = require("../../../DB/sample-db/schemas/CompanyModel");

async function getCompanies(req, res) {
  logger.init("get companies according to query");

  const schema = Joi.object({
    page: Joi.number().min(1),
    perPage: Joi.number().min(10).max(20),
    name: Joi.string().min(3).max(20),
    createdAt:Joi.string().isoDate(),
    status: Joi.string().valid('active', 'inactive'),
    type: Joi.array().items(Joi.string().valid('agent', 'contractor'))
    });


  const { value, error } = schema.validate(req.query);

  if (error) {
    throw new BadRequest(error.details[0].message );
  }
  const { perPage = 10, page = 1, name = null ,createdAt = null,status = null, type = [] } = value;
  const filter = companyMethods.createFilter(name,createdAt,status,type);
  const companies = await companyMethods.getCompanies(perPage,page,filter);
  const total = await Company.countDocuments(filter);
  const totalPage = Math.ceil(total/perPage) || 1;
  const photoUrl = getUrlForRequest(req);
  companyMethods.parseCompaniesPhotoUrl(companies,photoUrl);
  res.status(OK).json({ page, totalPage, perPage, total, companies});
  logger.success();
}

module.exports = {
    getCompanies,
};