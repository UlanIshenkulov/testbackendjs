const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const companyMethods = require("../../../DB/sample-db/methods/company");
const contactMethods = require("../../../DB/sample-db/methods/contact");
const { parseOne } = require("../companies.service");
const { getUrlForRequest } = require("../../../helpers/url.helper");
const { NotFound, BadRequest } = require("../../../constants/errors");
const Joi = require('joi');


/**
 * PATCH /companies/:id
 * Эндпоинт редактирования данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function editOne(req, res) {
  logger.init("edit company");
  const bodySchema = Joi.object({
    contactId: Joi.string().length(24).regex(/[0-9a-f]{24}/),
    name: Joi.string().min(3).max(20),
    shortName: Joi.string().min(3).max(10),
    businessEntity: Joi.string(),
    status: Joi.string().valid('active', 'inactive'),
    contract: Joi.object({
      no: Joi.string().required(),
      issue_date: Joi.string().isoDate().required(),
    }),
    type: Joi.array().items(Joi.string().valid('agent', 'contractor')),
    address:Joi.object({
        street: Joi.string().min(3).max(20).required(),
        city: Joi.string().min(3).max(20).required(),
        postalCode: Joi.string().min(3).max(20),
        country: Joi.string().min(3).max(20).required()
      })
  });

  const paramsSchema = Joi.object({
    id: Joi.string().length(24).required(),
    });

  const { value: body, error: bodyError } = bodySchema.validate(req.body);
  const { value: params, error: paramsError } = paramsSchema.validate(req.params);

  if (bodyError || paramsError) {
    throw new BadRequest(bodyError ? bodyError.details[0].message: paramsError.details[0].message );
  }

  if(body.contactId) {
    const contact = await contactMethods.getOne(body.contactId);
  
    if(!contact) {
      throw new NotFound( 'Contact does not found' );
    }
  }

  const company = await companyMethods.getOne(params.id);
  if (!company) {
    throw new NotFound("Company not found");
  }
  const updated = await companyMethods.editOne(params.id, body,company);
  if (!updated) {
    throw new NotFound('Company does not edited');
  }

  const photoUrl = getUrlForRequest(req);
  if(updated.photos.length > 0) {
    res.status(OK).json(parseOne(updated, photoUrl));
  }else {
    res.status(OK).json(updated);
  }
  logger.success();
}

module.exports = {
  editOne,
};
