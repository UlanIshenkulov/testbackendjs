module.exports = {
  ...require("./create-companies.action"),
  ...require("./get.companies.action"),
  ...require("./get-one.companies.action"),
  ...require("./edit-one.companies.action"),
  ...require("./add-image.companies.action"),
  ...require("./remove-image.companies.action"),
  ...require("./delete-companies.action"),
};
