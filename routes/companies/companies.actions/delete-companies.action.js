const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const companyMethods = require("../../../DB/sample-db/methods/company");
const { NotFound, BadRequest } = require("../../../constants/errors");
const Joi = require('joi');

async function deleteOne(req, res) {
  logger.init("delete company");
  const schema = Joi.object({
    id: Joi.string().length(24).regex(/[0-9a-f]{24}/).required(),
  });

  const {value, error} = schema.validate(req.params);

   if (error) {
    throw new BadRequest(error.details[0].message );
  }

  const deletedCompany = await companyMethods.deleteOne(value.id);
  
  if (!deletedCompany) {
    throw new NotFound("Company does not exist");
  }

  res.status(OK).json(deletedCompany);
  logger.success();
}

module.exports = {
    deleteOne,
};