const { Router } = require("express");
const actions = require("./contacts.actions");
const validator = require("./contacts.validator");

module.exports = Router()
  .get("/contacts/:id", actions.getOne)
  .patch("/contacts/:id", actions.editOne)
  .post("/contacts",actions.createContact)
  .delete("/contacts/:id", actions.deleteOne);
