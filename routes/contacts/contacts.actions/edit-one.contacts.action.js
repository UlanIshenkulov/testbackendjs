const logger = require("../../../services/logger.service")(module);
const { OK,CONFLICT } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/sample-db/methods/contact");
const { NotFound,BadRequest } = require("../../../constants/errors");
const Joi = require('joi');


/**
 * PATCH /contacts/:id
 * Эндпоинт редактирования данных контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function editOne(req, res) {
  logger.init("edit contact");

  const bodySchema = Joi.object({
    lastname: Joi.string().min(3).max(10),
    firstname: Joi.string().min(3).max(10),
    patronymic: Joi.string().min(3).max(10),
    phone: Joi.string().pattern(/^[0-9]+$/).min(6),
    email:Joi.string().email()
    });

  const paramsSchema = Joi.object({
    id: Joi.string().length(24).required(),
    });

  const { value: body, error: bodyError } = bodySchema.validate(req.body);
  const { value: params, error: paramsError } = paramsSchema.validate(req.params);

  if (bodyError || paramsError) {
    throw new BadRequest(bodyError ? bodyError.details[0].message: paramsError.details[0].message );
  }

  const updated = await contactMethods.editOne(params.id, body);

  if (!updated) {
    throw new NotFound();
  }

  res.status(OK).json(updated);
  logger.success();
}

module.exports = {
  editOne,
};
