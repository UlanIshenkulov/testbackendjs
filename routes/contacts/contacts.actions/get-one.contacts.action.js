const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/sample-db/methods/contact");
const { NotFound, BadRequest } = require("../../../constants/errors");
const Joi = require('joi');


/**
 * GET /contacts/:id
 * Эндпоинт получения данных контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function getOne(req, res) {
  logger.init("get contact");
  const schema = Joi.object({
    id: Joi.string().length(24).regex(/[0-9a-f]{24}/).required(),
  });

  const {value, error} = schema.validate(req.params);

   if (error) {
    throw new BadRequest(error.details[0].message );
  }

  const contact = await contactMethods.getOne(value.id);
  
  if (!contact) {
    throw new NotFound();
  }

  res.status(OK).json(contact);
  logger.success();
}

module.exports = {
  getOne,
};
