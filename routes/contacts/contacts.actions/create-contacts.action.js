const logger = require("../../../services/logger.service")(module);
const { CREATED } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/sample-db/methods/contact");
const { BadRequest, Conflict } = require("../../../constants/errors");
const Joi = require('joi');

async function createContact(req, res) {
  logger.init("create contact");

  const schema = Joi.object({
    lastname: Joi.string().min(3).max(10).required(),
    firstname: Joi.string().min(3).max(10).required(),
    patronymic: Joi.string().min(3).max(10).allow(''),
    phone: Joi.string().pattern(/^[0-9]+$/).min(6).required(),
    email:Joi.string().email().required(),
    });

   const {value, error} = schema.validate(req.body);

   if (error) {
      throw new BadRequest(error.details[0].message );
    }

  const isContactExists = await contactMethods.isContactExists(value.email);
  
  if(isContactExists) {
    throw new Conflict( 'Contact with this email already exists.' );
  }

  const newContact = await contactMethods.createContact(value);

  res.status(CREATED).json(newContact);
  logger.success();
}

module.exports = {
    createContact,
};