const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const contactMethods = require("../../../DB/sample-db/methods/contact");
const { NotFound, BadRequest } = require("../../../constants/errors");
const Joi = require('joi');

async function deleteOne(req, res) {
  logger.init("delete contact");
  const schema = Joi.object({
    id: Joi.string().length(24).regex(/[0-9a-f]{24}/).required(),
  });

  const {value, error} = schema.validate(req.params);

   if (error) {
    throw new BadRequest(error.details[0].message );
  }

  const deletedContact = await contactMethods.deleteOne(value.id);
  
  if (!deletedContact) {
    throw new NotFound();
  }

  res.status(OK).json(deletedContact);
  logger.success();
}

module.exports = {
    deleteOne,
};