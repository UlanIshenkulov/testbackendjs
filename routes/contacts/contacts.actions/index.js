module.exports = {
  ...require("./get-one.contacts.action"),
  ...require("./edit-one.contacts.action"),
  ...require("./create-contacts.action"),
  ...require("./delete-contacts.action"),
};
